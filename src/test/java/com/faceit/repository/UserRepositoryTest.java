package com.faceit.repository;

import com.faceit.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private UserRepository userRepository;

    @Before
    public void setUp() {
        User user = new User("John", "Doe", "jhonny", "1234", "jhonny@example.com", "UK");
        User user2 = new User("Jane", "Doe", "jane", "12345", "jane@example.com", "UK");
        User user3 = new User("Jane", "Doe", "jan", "12345", "jane2@example.com", "USA");
        User user4 = new User("Mark", "Doe", "jane", "12345", "mark@example.com", "USA");
        entityManager.persist(user);
        entityManager.persist(user2);
        entityManager.persist(user3);
        entityManager.persist(user4);
    }

    @Test
    public void testFindByEmail() {
        Optional<User> opuser = userRepository.findByEmail("jhonny@example.com");
        assertEquals("jhonny@example.com", opuser.get().getEmail());
    }

    @Test
    public void testFindByCountry() {
        List<User> opusers = userRepository.findAllByCountry("USA");
        assertEquals(2, opusers.size());
    }

    @Test
    public void testDeleteByEmail() {
        userRepository.deleteByEmail("jane2@example.com");
        assertEquals(1, userRepository.findAllByCountry("USA").size());
    }
}