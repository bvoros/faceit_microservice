package com.faceit.controller;

import com.faceit.service.EventService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.reactive.function.client.WebClient;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class EventControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EventService eventServiceMock;

    @Test
    public void getNewUserEvent_Call_Void() throws Exception {
        this.mockMvc.perform(get("/events/newuser")).andDo(print()).andExpect(status().isOk()).andDo(document("getNewUserEvents"));
    }

    @Test
    public void getNicknameChangeEvent_Call_Void() throws Exception {
        this.mockMvc.perform(get("/events/nicknamechange")).andDo(print()).andExpect(status().isOk()).andDo(document("getNicknameChangeEvent"));
    }
}
