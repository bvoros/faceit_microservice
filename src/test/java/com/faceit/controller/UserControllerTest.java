package com.faceit.controller;

import com.faceit.model.User;
import com.faceit.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userServiceMock;


    @Test
    public void getUsersByCountry_ValidData_ReturnsUsers() throws Exception {
        User user = new User("John2", "Doe2", "jhonny2", "12342", "jhonny@example.com", "USA");
        Mockito.when(userServiceMock.getUsersByCountry("USA")).thenReturn(Arrays.asList(user));
        this.mockMvc.perform(get("/users/filter/country/USA")).andDo(print()).andExpect(status().isOk()).
                andExpect(content().string(containsString("jhonny@example.com"))).andDo(document("getUsersByCountry"));
    }

    @Test
    public void createUser_ValidData_ReturnsOK() throws Exception {
        User user = new User("John2", "Doe2", "jhonny2", "12342", "jhonny@example.com", "USA");
        Mockito.when(userServiceMock.createUser(user)).thenReturn(new ResponseEntity<String>("User Created!", HttpStatus.OK));
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(user)))
                .andExpect(status().isOk()).andDo(document("createUser"));
    }

    @Test
    public void getUser_ValidData_ReturnsUser() throws Exception {
        User user = new User("John2", "Doe2", "jhonny2", "12342", "jhonny@example.com", "USA");
        Mockito.when(userServiceMock.getUser("jhonny@example.com")).thenReturn(java.util.Optional.ofNullable(user));
        this.mockMvc.perform(get("/users/jhonny@example.com")).andDo(print()).andExpect(status().isOk()).andExpect(content()
                .string(containsString("jhonny@example.com"))).andDo(document("getUser"));
    }

    @Test
    public void updateUser_ValidData_ReturnsOK() throws Exception {
        User user = new User("John2", "Doe2", "jhonny2", "12342", "jhonny@example.com", "USA");
        Mockito.when(userServiceMock.updateUser(user, "jhonny@example.com")).thenReturn(new ResponseEntity<String>("User Updated!", HttpStatus.OK));
        this.mockMvc.perform(put("/users/jhonny@example.com")
                .contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(user)))
                .andExpect(status().isOk()).andDo(document("updateUser"));
    }

    @Test
    public void deleteUser_ValidData_ReturnsOK() throws Exception {
        this.mockMvc.perform(delete("/users/jhonny@example.com")).andExpect(status().isOk()).andDo(document("deleteUser"));
    }

}