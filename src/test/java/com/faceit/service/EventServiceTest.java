package com.faceit.service;

import com.faceit.event.NewUserEvent;
import com.faceit.event.NicknameChangeEvent;
import com.faceit.exception.UserAlreadyExistsException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class EventServiceTest {

    @MockBean
    EventService eventServiceMock;
    @Mock
    NewUserEvent newUserEvent;
    @Mock
    NicknameChangeEvent nicknameChangeEvent;
    @Mock
    SseEmitter sseEmitterMock;
    private EventService eventService;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Before
    public void setUp() {
        eventService = new EventService();
    }

    @Test
    public void onNewUser_ValidData_Listens() throws IOException {
        applicationEventPublisher.publishEvent(newUserEvent);
        Mockito.verify(eventServiceMock, Mockito.times(1)).onNewUser(newUserEvent);
    }

    @Test
    public void oonNicknameChange_ValidData_Listens() throws IOException {
        applicationEventPublisher.publishEvent(nicknameChangeEvent);
        Mockito.verify(eventServiceMock, Mockito.times(1)).onNicknameChange(nicknameChangeEvent);
    }

    @Test
    public void getNewUserEvents_ValidData_CreatesEmitter() throws Exception {
        eventService.getNewUserEvents(new MockHttpServletResponse());
        assertEquals(eventService.getNewUserEmitters().size(), 1);
    }

    @Test
    public void getNicknameChangeEvents_ValidData_CreatesEmitter() throws Exception {
        eventService.getNicknameChangeEvents(new MockHttpServletResponse());
        assertEquals(eventService.getNicknameChangemitters().size(), 1);
    }

    @Test
    public void onNewUser_ValidData_SendsNewUserToEmitters() throws IOException {
        eventService.getNewUserEmitters().add(sseEmitterMock);
        eventService.onNewUser(newUserEvent);
        Mockito.verify(sseEmitterMock, Mockito.times(1)).send(newUserEvent);
    }

    @Test
    public void onNicknameChange_ValidData_SendsNewUserToEmitters() throws IOException {
        eventService.getNicknameChangemitters().add(sseEmitterMock);
        eventService.onNicknameChange(nicknameChangeEvent);
        Mockito.verify(sseEmitterMock, Mockito.times(1)).send(nicknameChangeEvent);
    }

    @Test
    public void onNewUser_ExceptionOccurs_DeletesEmitter() throws IOException {
        Mockito.doThrow(new IOException()).when(sseEmitterMock).send(newUserEvent);
        eventService.getNewUserEmitters().add(sseEmitterMock);
        assertFalse(eventService.getNewUserEmitters().isEmpty());
        eventService.onNewUser(newUserEvent);
        assertTrue(eventService.getNewUserEmitters().isEmpty());
    }

    @Test
    public void onNicknameChange_ExceptionOccurs_DeletesEmitter() throws IOException {
        Mockito.doThrow(new IOException()).when(sseEmitterMock).send(nicknameChangeEvent);
        eventService.getNicknameChangemitters().add(sseEmitterMock);
        assertFalse(eventService.getNicknameChangemitters().isEmpty());
        eventService.onNicknameChange(nicknameChangeEvent);
        assertTrue(eventService.getNicknameChangemitters().isEmpty());
    }


}
