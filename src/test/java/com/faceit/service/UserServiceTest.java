package com.faceit.service;

import com.faceit.exception.UserAlreadyExistsException;
import com.faceit.exception.UserNotFoundException;
import com.faceit.model.User;
import com.faceit.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class UserServiceTest {

    @TestConfiguration
    static class UserServiceTestContextConfiguration {

        @Bean
        public UserService userService() {
            return new UserService();
        }

        @Bean
        public BCryptPasswordEncoder bCryptPasswordEncoder() {
            return new BCryptPasswordEncoder();
        }
    }

    @Autowired
    UserService userService;
    @MockBean
    private UserRepository userRepositoryMock;

    @Before
    public void setUp() {
        User user = new User("John", "Doe", "jhonny", "1234", "jhonny@example.com", "UK");
        User user2 = new User("John2", "Doe2", "jhonny2", "12342", "jhonny2@example.com", "USA2");
        Mockito.when(userRepositoryMock.findByEmail("jhonny@example.com")).thenReturn(java.util.Optional.ofNullable(user));
        Mockito.when(userRepositoryMock.findByEmail("jane2@example.com")).thenReturn(java.util.Optional.empty());
        Mockito.when(userRepositoryMock.findByEmail("jhonny2@example.com")).thenReturn(java.util.Optional.ofNullable(user2));
    }

    @Test
    public void getUser_ValidData_ReturnsUser() {
        Optional<User> foundUser = userService.getUser("jhonny@example.com");
        assertEquals("jhonny@example.com", foundUser.get().getEmail());
    }

    @Test(expected = UserNotFoundException.class)
    public void getUser_MissingUser_ThrowsException() throws Exception {
        Optional<User> foundUser = userService.getUser("jane2@example.com");
    }

    @Test
    public void createUser_ValidData_ReturnsCreated() throws Exception {
        User user = new User("Jane", "Doe", "jane", "12345", "jane@example.com", "UK");
        ResponseEntity<String> responseEntity = userService.createUser(user);
        Mockito.verify(userRepositoryMock, Mockito.times(1)).save(user);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED);
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void createUser_ExistingUser_ThrowsException() throws Exception {
        User user = new User("John", "Doe", "jhonny", "1234", "jhonny@example.com", "UK");
        ResponseEntity<String> responseEntity = userService.createUser(user);
    }

    @Test
    public void deleteByEmail_ValidData_ReturnsOK() throws Exception {
        ResponseEntity<String> responseEntity = userService.deleteUser("jhonny@example.com");
        Mockito.verify(userRepositoryMock, Mockito.times(1)).deleteByEmail("jhonny@example.com");
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test(expected = UserNotFoundException.class)
    public void deleteByEmail_MissingUser_ThrowsException() throws Exception {
        ResponseEntity<String> responseEntity = userService.deleteUser("jane@example.com");
    }

    @Test
    public void updateUser_ValidData_ReturnsOK() throws Exception {
        User user = new User("John2", "Doe2", "jhonny2", "12342", "jhonny@example.com", "USA2");
        ResponseEntity<String> responseEntity = userService.updateUser(user, "jhonny@example.com");
        Mockito.verify(userRepositoryMock, Mockito.times(1)).save(user);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test(expected = UserNotFoundException.class)
    public void updateUser_updateNonExistingUser_ThrowsException() throws Exception {
        User user = new User("John2", "Doe2", "jhonny2", "12342", "jhonny@example.com", "USA2");
        ResponseEntity<String> responseEntity = userService.updateUser(user, "jane@example.com");
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void updateUser_updateEmailToExistingEmail_ThrowsException() throws Exception {
        User user = new User("John", "Doe", "jhonny", "1234", "jhonny@example.com", "USA");
        ResponseEntity<String> responseEntity = userService.updateUser(user, "jhonny2@example.com");
    }

    @Test
    public void getUsersByCountry_ValidData_ReturnsUsers() throws Exception {
        User user = new User("Jane", "Doe", "jan", "12345", "jane2@example.com", "USA");
        User user2 = new User("Jane", "Doe", "jan", "12345", "jane2@example.com", "USA");
        Mockito.when(userRepositoryMock.findAllByCountry("USA")).thenReturn(Arrays.asList(user, user2));
        List<User> users = userService.getUsersByCountry("USA");
        assertEquals(users.size(), 2);
    }

}