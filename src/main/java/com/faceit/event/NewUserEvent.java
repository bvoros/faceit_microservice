package com.faceit.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class NewUserEvent extends ApplicationEvent {
    private String email;

    public NewUserEvent(Object source, String email) {
        super(source);
        this.email = email;
    }

}