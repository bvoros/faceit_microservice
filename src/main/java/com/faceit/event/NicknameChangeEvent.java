package com.faceit.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class NicknameChangeEvent extends ApplicationEvent {

    private String email;
    private String newNickname;

    public NicknameChangeEvent(Object source, String email, String newNickname) {
        super(source);
        this.email = email;
        this.newNickname = newNickname;
    }

}