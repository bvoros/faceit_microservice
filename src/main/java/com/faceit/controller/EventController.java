package com.faceit.controller;

import com.faceit.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.CopyOnWriteArrayList;

@RestController
public class EventController {

    private final CopyOnWriteArrayList<SseEmitter> newUserEmitters = new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<SseEmitter> nicknameChangemitters = new CopyOnWriteArrayList<>();

    @Autowired
    private EventService eventService;

    @GetMapping("/events/newuser")
    public SseEmitter getNewUserEvents(HttpServletResponse response) {
        return eventService.getNewUserEvents(response);
    }

    @GetMapping("/events/nicknamechange")
    public SseEmitter getNicknameChangeEvents(HttpServletResponse response) {
        return eventService.getNicknameChangeEvents(response);
    }

}