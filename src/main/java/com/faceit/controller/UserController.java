package com.faceit.controller;

import com.faceit.model.User;
import com.faceit.repository.UserRepository;
import com.faceit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @PostMapping("/users")
    public ResponseEntity<String> createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @GetMapping("/users/{email}")
    @ResponseBody
    public Optional<User> getUser(@PathVariable String email) {
        return userService.getUser(email);
    }

    @GetMapping("/users/filter/country/{country}")
    @ResponseBody
    public List getUsersByCountry(@PathVariable String country) {
        return userService.getUsersByCountry(country);
    }

    @PutMapping("/users/{email}")
    public ResponseEntity<String> updateUser(@RequestBody User user, @PathVariable String email) {
        return userService.updateUser(user, email);
    }

    @DeleteMapping("/users/{email}")
    public ResponseEntity<String> deleteUser(@PathVariable String email) {
        return userService.deleteUser(email);
    }

}