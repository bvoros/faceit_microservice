package com.faceit.service;

import com.faceit.event.NewUserEvent;
import com.faceit.event.NicknameChangeEvent;
import com.faceit.repository.UserRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import reactor.core.publisher.Flux;

import javax.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
@Service
@Getter
public class EventService {

    public final CopyOnWriteArrayList<SseEmitter> newUserEmitters = new CopyOnWriteArrayList<>();
    public final CopyOnWriteArrayList<SseEmitter> nicknameChangemitters = new CopyOnWriteArrayList<>();

    public SseEmitter getNewUserEvents(HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-store");

        SseEmitter emitter = new SseEmitter();
        log.info(String.format("new Emitter has been created: %s", emitter.toString()));
        this.newUserEmitters.add(emitter);

        emitter.onCompletion(() -> this.newUserEmitters.remove(emitter));
        emitter.onTimeout(() -> this.newUserEmitters.remove(emitter));

        return emitter;
    }

    public SseEmitter getNicknameChangeEvents(HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-store");

        SseEmitter emitter = new SseEmitter();
        log.info(String.format("new Emitter has been created: %s", emitter.toString()));
        this.nicknameChangemitters.add(emitter);

        emitter.onCompletion(() -> this.nicknameChangemitters.remove(emitter));
        emitter.onTimeout(() -> this.nicknameChangemitters.remove(emitter));

        return emitter;
    }

    @EventListener
    public void onNewUser(NewUserEvent newUserEvent) {
        List<SseEmitter> deadEmitters = new ArrayList<>();
        log.info(String.format("NewUser event has been called for user: %s", newUserEvent.getEmail()));
        this.newUserEmitters.forEach(emitter -> {
            try {
                emitter.send(newUserEvent);
            } catch (Exception e) {
                log.error("Cannot send data to emitter", e);
                deadEmitters.add(emitter);
            }
        });
        this.newUserEmitters.removeAll(deadEmitters);
    }

    @EventListener
    public void onNicknameChange(NicknameChangeEvent nicknameChangeEvent) {
        List<SseEmitter> deadEmitters = new ArrayList<>();
        this.nicknameChangemitters.forEach(emitter -> {
            try {
                emitter.send(nicknameChangeEvent);
            } catch (Exception e) {
                log.error("Cannot send data to emitter", e);
                deadEmitters.add(emitter);
            }
        });
        log.info(String.format("NicknameChange event has been called for user: %s", nicknameChangeEvent.getEmail()));
        this.nicknameChangemitters.removeAll(deadEmitters);
    }
}