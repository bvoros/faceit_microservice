package com.faceit.service;

import com.faceit.event.NewUserEvent;
import com.faceit.event.NicknameChangeEvent;
import com.faceit.exception.UserAlreadyExistsException;
import com.faceit.exception.UserNotFoundException;
import com.faceit.model.User;
import com.faceit.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public ResponseEntity<String> createUser(User user) {
        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            log.error(String.format("User already exists. Email: '%s'", user.getEmail()));
            throw new UserAlreadyExistsException(user.getEmail());
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        NewUserEvent newUserEvent = new NewUserEvent(UserService.class, user.getEmail());
        applicationEventPublisher.publishEvent(newUserEvent);
        log.info(String.format("New user have been created. Email: %s", user.getEmail()));
        return new ResponseEntity<String>("User Created!", HttpStatus.CREATED);
    }

    public Optional<User> getUser(String email) {
        Optional<User> user = userRepository.findByEmail(email);
        if (!user.isPresent()) {
            log.error(String.format("User not found. Email: '%s'", email));
            throw new UserNotFoundException(email);
        }
        log.info(String.format("User have been found. Email: %s", email));
        return userRepository.findByEmail(email);
    }

    public List getUsersByCountry(String country) {
        List<User> users = userRepository.findAllByCountry(country);
        log.info(String.format("Users found by country. Country: %s. Number: %d", country, users.size()));
        return users;
    }

    public ResponseEntity<String> updateUser(User user, String email) {
        Optional<User> userOptional = userRepository.findByEmail(email);
        if (!userOptional.isPresent()) {
            log.error(String.format("User not found. Email: '%s'", email));
            throw new UserNotFoundException(email);
        }
        if (!email.equals(user.getEmail()) && userRepository.findByEmail(user.getEmail()).isPresent()) {
            log.error(String.format("User already exists. Email: '%s'", user.getEmail()));
            throw new UserAlreadyExistsException(user.getEmail());
        }
        user.setId(userOptional.get().getId());
        if (!user.getNickName().equals(userOptional.get().getNickName())) {
            NicknameChangeEvent nicknameChangeEvent = new NicknameChangeEvent(UserService.class, email, user.getNickName());
            applicationEventPublisher.publishEvent(nicknameChangeEvent);
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        log.info(String.format("User updated. Email: %s", email));
        return new ResponseEntity<String>("User Updated!", HttpStatus.OK);
    }

    @Transactional
    public ResponseEntity<String> deleteUser(String email) {
        if (!userRepository.findByEmail(email).isPresent()) {
            log.error(String.format("User not found. Email: '%s'", email));
            throw new UserNotFoundException(email);
        }
        userRepository.deleteByEmail(email);
        log.info(String.format("User deleted. Email: %s", email));
        return new ResponseEntity<String>("User Deleted!", HttpStatus.OK);
    }

}