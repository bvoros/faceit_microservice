package com.faceit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class UserAlreadyExistsException extends RuntimeException {

    private String email;

    public UserAlreadyExistsException(String email) {
        super(String.format("User already exists. Email: '%s'", email));
        this.email = email;
    }
}
