package com.faceit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

    private String email;

    public UserNotFoundException(String email) {
        super(String.format("User not found. Email: '%s'", email));
        this.email = email;

    }
}