# FACEIT_MICROSERVICE
Requires maven 3.0+

Using Springboot, Hibernate, Lombok, H2database. JUnit and Mockito for testing.

Clone or download the repository and open it.

Application start on localhost from command line: mvn spring-boot:run

Making a package will run tests and generate snippets with possible endpoints.

Making package: mvn clean package spring-boot:repackage

Spring-boot actuator provides health checks, default configuration.

Project is using lombok, plugin could be needed for IDE.

Example body to create a user on POST /users:

    "firstName": "Jhon",
    "lastName": "Doe",
    "nickName": "Jhonny",
    "password": "1234",
    "email": "jhonny@mail.com",
    "country": "USA"


Restrictions:
* Cannot create user with existing email
* Cannot update email to eyisting email

The application uses h2 database:
* h2 console path: /h2
* Driver class: org.h2.Driver
* JDBC URL: jdbc:h2:mem:testdb
* User name: sa

Assumptions:
* Email is unique, other attributes not
* Delete and update is based on email
* Password should be encrypted
* Password should not be visible in a response
* Other services can listen to emitted events

Possible extensions or improvements:
* Follow HATEOAS. Now for simplicity the mapped urls return only a status. 
To achieve a fully REST like architecture, dynamic references should be added to responses.
* Security modul now is only used for password encryption, authorization and authentication should be 
implemented.
* Integration tests with test data in database
* Map PATCH to update a single attribute. Now UPDATE always refreshes the user.
* Add salt to password encryption
* Check validity of data: valid email, valid country code etc...
* Implement error page



